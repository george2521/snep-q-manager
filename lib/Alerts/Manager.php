<?php

/**
 * Class to manager Alerts in mongodb
 *
 * @see Q-Manager
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 Opens Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 *
 */
class Alerts_Manager {

    /**
     * Method to get all alerts
     * @return <array>
     */
    public static function getAll($url) {

        $http = curl_init($url);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
        $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
        $http_response = curl_exec($http);
        $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        switch ($httpcode) {
            case 200:
                $data = json_decode($http_response);
                break;
            default:
                $data = $httpcode;
                break;
        }

            return $data;
    }

    /**
     * Method to get alert
     * @param <int> $id
     * @return <array>
     */
    public static function getAlert($url) {

        $http = curl_init($url);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
        $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
        $http_response = curl_exec($http);
        $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        switch ($httpcode) {
            case 200:
                $data = json_decode($http_response);
                break;
            default:
                $data = $httpcode;
                break;
        }

            return $data;
    }

    /**
     * Method to update a alert data
     * @param <Array> $alert
     */
    public function add($alert, $url) {

        $data = array("queue" => trim($alert["name"]), "created" => date('Y-m-d H:i:s'), "updated" => date('Y-m-d H:i:s'));
        $add = false;

        if(isset($alert["view_active"])){
            $add = true;
            $data["alerts"]["view"]["state"] = "true";
            $data["alerts"]["view"]["tme"]   = (isset($alert["view_tme"])) ? trim($alert["view_tme"]) : "1";
            $data["alerts"]["view"]["sla"]   = (isset($alert["view_sla"])) ? trim($alert["view_sla"]) : "1";
        }

        if(isset($alert["sound_active"])){
            $add = true;
            $data["alerts"]["sound"]["state"] = "true";
            $data["alerts"]["sound"]["tme"]   = (isset($alert["sound_tme"])) ? trim($alert["sound_tme"]) : "1";
            $data["alerts"]["sound"]["sla"]   = (isset($alert["sound_sla"])) ? trim($alert["sound_sla"]) : "1";
        }

        if(isset($alert["mail_active"])){
            $add = true;
            $data["alerts"]["mail"]["state"] = "true";
            $data["alerts"]["mail"]["tme"]   = (isset($alert["mail_tme"])) ? trim($alert["mail_tme"]) : "1";
            $data["alerts"]["mail"]["sla"]   = (isset($alert["mail_sla"])) ? trim($alert["mail_sla"]) : "1";
            $data["alerts"]["mail"]["destination"]   = (isset($alert["mail_email"])) ? trim($alert["mail_email"]) : "";
        }

        if($add){

            $content = json_encode($data);
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

            $json_response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            return $httpcode;
        }else{
            return 200;
        }

    }

    /**
     * Method to remove a alert
     * @param <string> $queue
     */
    public function remove($queue, $url) {

        $array = array("queue" => $queue);
        $content = json_encode($array);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;
    }
}