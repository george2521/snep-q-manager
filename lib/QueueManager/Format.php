<?php

/**
 * Class format data for reports in QueueManager
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 Opens Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 *
 */
class QueueManager_Format {

	/**
	 * get function for format data
	 * @param <string> $reportName - Name of the report. Example: dac
	 * @param <string> $type - Type of the report. Example: syntetic
	 * @param <obj> $data - Obj with data
	 * @return <json> $result
	 */
		public static function formatter($reportName, $type, $data){

			$function =  $reportName.$type;
			$result = self::$function($data);

			return $result;
		}

		/**
		 * add class bootstrap in event
		 * @param <string> $event
		 * @return <string> $class
		 */
		public static function labelEvent($event){

				switch ($event) {
						case 'enterqueue':
								$class = "label label-primary";
								break;
						case "answered":
								$class = 'label label-success';
								break;
						case "answer":
								$class = 'label label-success';
								break;
						case "login":
								$class = 'label label-success';
								break;
						case "ringnoanswer":
								$class = 'label label-danger';
								break;
						case "noanswer":
								$class = 'label label-danger';
								break;
						case "queuetimeout":
								$class = 'label label-danger';
								break;
						case "logoff":
								$class = 'label label-danger';
								break;
						case 'abandon':
								$class = 'label label-warning';
								break;
						case 'queueabandon':
								$class = 'label label-warning';
								break;
						case 'pause':
								$class = 'label label-warning';
								break;
						case 'transfer':
								$class = 'label label-info';
								break;
						case 'caller':
								$class = 'label label-success';
								break;
						case 'agent':
								$class = 'label label-warning';
								break;
						default:
								$class = 'label label-default';
								break;
				}

				return $class;
		}

		/**
	 * format data for report dac analytic
	 * @param <obj> $data - Obj with data
	 * @return <obj> $result
	 */
		public static function dacanalytic($data){

			$i18n = Zend_Registry::get("i18n");
			date_default_timezone_set('UTC');
			$result = array();

			foreach($data as $queue => $item){
			    if(isset($item->message)){
					    $result[$queue] = false;
					}else{
					    foreach($item as $x => $call){

							    // header
									$result[$queue][$x]['header']['calldate'] = date("d/m/Y H:i:s", strtotime($call->calldate));
									$result[$queue][$x]['header']['queue'] = $call->queue;
									$result[$queue][$x]['header']['from'] = $call->from;
									$result[$queue][$x]['header']['firstevent'] = $i18n->translate("enterqueue");
									$result[$queue][$x]['header']['holdtime'] = "00:00:00";

									// footer
									$result[$queue][$x]['footer']['callid'] = $call->callid;
									$result[$queue][$x]['footer']['lastevent'] = $call->event;
									$result[$queue][$x]['footer']['hangupdate'] = date("d/m/Y H:i:s", strtotime($call->hangupdate));
									$result[$queue][$x]['footer']['duration'] = QueueManager_Functions::fmt_Time($call->duration);
									// footer call abandon
									$result[$queue][$x]['footer']['talktime'] = QueueManager_Functions::fmt_Time(0);
									$result[$queue][$x]['footer']['timeWaiting'] = QueueManager_Functions::fmt_Time($call->duration);

									if(isset($call->audio)){
									    $result[$queue][$x]['footer']['record'] = $call->audio->url.$call->audio->file;
									}

									// historical call in queue
							    foreach($call->callflow as $y => $callflow){

									    if($callflow->status == 'caller'){
											    $result[$queue][$x]['callflow'][$y]['event'] = $i18n->translate("Off by the client");
													$result[$queue][$x]['callflow'][$y]['holdtime'] = QueueManager_Functions::fmt_Time($callflow->talktime);
													$result[$queue][$x]['footer']['talktime'] = QueueManager_Functions::fmt_Time($callflow->talktime);

											}elseif($callflow->status == 'agent'){
											    $result[$queue][$x]['callflow'][$y]['event'] = $i18n->translate("Off by the operator");
													$result[$queue][$x]['footer']['talktime'] = QueueManager_Functions::fmt_Time($callflow->talktime);
													$result[$queue][$x]['callflow'][$y]['holdtime'] = QueueManager_Functions::fmt_Time($callflow->talktime);

											}else{
											    $result[$queue][$x]['callflow'][$y]['event'] = $callflow->status;
													$result[$queue][$x]['callflow'][$y]['holdtime'] = QueueManager_Functions::fmt_Time($callflow->holdtime);

													if($callflow->status == 'answered'){
													    $result[$queue][$x]['footer']['timeWaiting'] = QueueManager_Functions::fmt_Time($callflow->holdtime);
													}
											}

											$result[$queue][$x]['callflow'][$y]['class'] = self::labelEvent($callflow->status);
											$result[$queue][$x]['callflow'][$y]['queue'] = $call->queue;
							        $result[$queue][$x]['callflow'][$y]['eventdate'] = date("d/m/Y H:i:s", strtotime($callflow->eventdate));

											(isset($callflow->ringtime)) ? $result[$queue][$x]['callflow'][$y]['ringTime'] = QueueManager_Functions::fmt_Time($callflow->ringtime) : $result[$queue][$x]['callflow'][$y]['ringTime'] = "-";
											(isset($callflow->endpoint)) ? $result[$queue][$x]['callflow'][$y]['endpoint'] = $callflow->endpoint : $result[$queue][$x]['callflow'][$y]['endpoint'] = "-";
											(isset($callflow->name)) ? $result[$queue][$x]['callflow'][$y]['name'] = $callflow->name : $result[$queue][$x]['callflow'][$y]['name'] = "-";
											$holdtime = $callflow->holdtime;

							    }

									// event queue timeout
									if($call->event == 'queuetimeout' || $call->event == 'queueabandon'){
									    if(!isset($y)){
											    $y = 0;
											}

											$result[$queue][$x]['callflow'][$y+1]['event'] = $call->event;
											$result[$queue][$x]['callflow'][$y+1]['queue'] = $call->queue;
											$result[$queue][$x]['callflow'][$y+1]['class'] = self::labelEvent($call->event);
											$result[$queue][$x]['callflow'][$y+1]['holdtime'] =  $result[$queue][$x]['footer']['duration'];
											$result[$queue][$x]['callflow'][$y+1]['eventdate'] =  date("d/m/Y H:i:s", strtotime($call->hangupdate));
									}

						 }
			   };
			}

			$result = json_encode($result);
			return $result;

		}

		/**
	 * format data for report dac syntetic
	 * @param <obj> $data - Obj with data
	 * @return <obj> $result
	 */
		public static function dacsyntetic($data){

				date_default_timezone_set('UTC');
			$result = array();
				foreach($data as $queue => $item){

						if(isset($item->message)){
								$result[$queue] = false;
						}else{

								$result[$queue]['abandon'] = 0; $result[$queue]['answered'] = 0; $result[$queue]['timeout'] = 0;
								$result[$queue]['transfer'] = 0; $result[$queue]['agent'] = 0; $result[$queue]['caller'] = 0;
								$result[$queue]['talktime'] = 0; $result[$queue]['holdtime'] = 0; $result[$queue]['duration'] = 0;

								foreach($item as $x => $call){

										$date = date("d/m/Y", strtotime($call->calldate));
										$hour = date("H", strtotime($call->calldate));

										// inicializing variable for item have value
										if(!isset($result[$queue]['parciais'][$date][$hour]['abandon'])){ $result[$queue]['parciais'][$date][$hour]['abandon'] = 0; };
										if(!isset($result[$queue]['parciais'][$date][$hour]['timeout'])){ $result[$queue]['parciais'][$date][$hour]['timeout'] = 0; };
										if(!isset($result[$queue]['parciais'][$date][$hour]['answered'])){ $result[$queue]['parciais'][$date][$hour]['answered'] = 0; };
										if(!isset($result[$queue]['parciais'][$date][$hour]['duration'])){ $result[$queue]['parciais'][$date][$hour]['duration'] = 0; };
										if(!isset($result[$queue]['parciais'][$date][$hour]['total'])){ $result[$queue]['parciais'][$date][$hour]['total'] = 0; };

										if(!isset($result[$queue]['graphics'][$date]['total'])){ $result[$queue]['graphics'][$date]['total'] = 0; };
										if(!isset($result[$queue]['graphics'][$date]['abandon'])){ $result[$queue]['graphics'][$date]['abandon'] = 0; };
										if(!isset($result[$queue]['graphics'][$date]['timeout'])){ $result[$queue]['graphics'][$date]['timeout'] = 0; };
										if(!isset($result[$queue]['graphics'][$date]['answered'])){ $result[$queue]['graphics'][$date]['answered'] = 0; };
										if(!isset($result[$queue]['totalhours'][$hour])){ $result[$queue]['totalhours'][$hour] = 0; };

										$result[$queue]['total'] = count($item);
										$result[$queue]['parciais'][$date][$hour]['duration'] += $call->duration;
										$result[$queue]['totalhours'][$hour]++;

										// event call
										switch ($call->event) {
												case 'queueabandon':
														$result[$queue]['abandon']++;

														// parcials
														$result[$queue]['parciais'][$date][$hour]['abandon']++ ;
														$result[$queue]['parciais'][$date][$hour]['total']++;
														(isset($result[$queue]['max'][$date][$hour])) ? $result[$queue]['max'][$date][$hour]++ : $result[$queue]['max'][$date][$hour] = 1;

														// graphics
														$result[$queue]['graphics'][$date]['total']++;
														$result[$queue]['graphics'][$date]['abandon']++;
														break;
												case 'queuetimeout':
														$result[$queue]['timeout']++;

														// parcials
														$result[$queue]['parciais'][$date][$hour]['timeout']++;
														$result[$queue]['parciais'][$date][$hour]['total']++;
														(isset($result[$queue]['max'][$date][$hour])) ? $result[$queue]['max'][$date][$hour]++ : $result[$queue]['max'][$date][$hour] = 1;

														// graphics
														$result[$queue]['graphics'][$date]['total']++;
														$result[$queue]['graphics'][$date]['timeout']++;
														break;
												case 'queuehangup':
														$result[$queue]['answered']++;

														// parcials
														$result[$queue]['parciais'][$date][$hour]['answered']++;
														$result[$queue]['parciais'][$date][$hour]['total']++;
														(isset($result[$queue]['max'][$date][$hour])) ? $result[$queue]['max'][$date][$hour]++ : $result[$queue]['max'][$date][$hour] = 1;

														// graphics
														$result[$queue]['graphics'][$date]['total']++;
														$result[$queue]['graphics'][$date]['answered']++;
														break;
										}

										// data call answered(transfer,agent and caller)
										if(isset($call->hangupreason)){
												$result[$queue][$call->hangupreason]++;
										}

										foreach ($call->callflow as $key => $callflow) {

												$result[$queue]['talktime'] += (isset($callflow->talktime)) ? $callflow->talktime : 0 ;
												// last holdtime in call
												$holdtime = (isset($callflow->holdtime)) ? $callflow->holdtime : 0 ;
												$hold[$date][$hour]['holdtime'] = (isset($callflow->holdtime)) ? $callflow->holdtime : 0 ;

												//parciais
												if(!isset($result[$queue]['parciais'][$date][$hour]['talktime'])){
														$result[$queue]['parciais'][$date][$hour]['talktime'] = 0;
												}
												$result[$queue]['parciais'][$date][$hour]['talktime'] += (isset($callflow->talktime)) ? $callflow->talktime : 0 ;
										}

										$result[$queue]['duration'] += $call->duration;

										if($call->event == 'queueabandon' || $call->event == 'queuetimeout'){
												$result[$queue]['holdtime'] += $call->duration;
										}else{
											// holdtime value
											$result[$queue]['holdtime'] += (isset($holdtime)) ? $holdtime : 0 ;
										}

										// parcials holdtime
										if(!isset($result[$queue]['parciais'][$date][$hour]['holdtime'])){
												$result[$queue]['parciais'][$date][$hour]['holdtime'] = (isset($hold[$date][$hour]['holdtime'])) ? $hold[$date][$hour]['holdtime'] : 0;
										}else{
												$result[$queue]['parciais'][$date][$hour]['holdtime'] += (isset($hold[$date][$hour]['holdtime'])) ? $hold[$date][$hour]['holdtime'] : 0;
										}

								}

								// max call in data - tytpe totalization
								foreach($result[$queue]['max'] as $data => $val){
										$max =  max($val);
										$result[$queue]['parciais'][$data]['max'] = $max;
								}
								unset($result[$queue]['max']);
								ksort($result[$queue]['totalhours']);
						}
				}

				$result = json_encode($result);
				return $result;
		}

		public static function operatorview($data){

				date_default_timezone_set('UTC');

				foreach ($data as $operator => $events) {
						if(isset($events->message)){
								$result[$operator] = false;
						}else{

								// get value worktime of the operator
								$urlConnector = "http://127.0.0.1:3000/config/agents";

								$url = $urlConnector."/?agent=".$operator;
								$agent = Operators_Manager::getAgentByCode($url);

								foreach($events as $x =>$event){

										$date = date("d/m/Y", strtotime($event->eventdate));
										$hour = date("H:i:s", strtotime($event->eventdate));
										$eventdate = date("d/m/Y H:i:s", strtotime($event->eventdate));

										// events type
										if($event->event->type == "call"){

												// total
												(isset($result[$event->agent]['total'][$event->event->content->type]['total'])) ? $result[$event->agent]['total'][$event->event->content->type]['total']++ : $result[$event->agent]['total'][$event->event->content->type]['total'] = 1;
												// parcials
												(isset($result[$event->agent]['parciais'][$date][$event->event->content->type]['total'])) ? $result[$event->agent]['parciais'][$date][$event->event->content->type]['total']++ : $result[$event->agent]['parciais'][$date][$event->event->content->type]['total'] = 1;

												//total by type(answered, noanswer)
												(isset($result[$event->agent]['total'][$event->event->content->type][$event->event->content->status])) ?  $result[$event->agent]['total'][$event->event->content->type][$event->event->content->status]++ : $result[$event->agent]['total'][$event->event->content->type][$event->event->content->status] = 1;
												(isset($result[$event->agent]['parciais'][$date][$event->event->content->type][$event->event->content->status])) ?  $result[$event->agent]['parciais'][$date][$event->event->content->type][$event->event->content->status]++ : $result[$event->agent]['parciais'][$date][$event->event->content->type][$event->event->content->status] = 1;

												// times talktime, ringtime and duration
												if(!isset($event->event->content->talktime)){
														$event->event->content->talktime = 0;
												}
												(isset($result[$event->agent]['total'][$event->event->content->type]['talktime'])) ? $result[$event->agent]['total'][$event->event->content->type]['talktime'] += $event->event->content->talktime : $result[$event->agent]['total'][$event->event->content->type]['talktime'] = $event->event->content->talktime;
												(isset($result[$event->agent]['parciais'][$date][$event->event->content->type]['talktime'])) ? $result[$event->agent]['parciais'][$date][$event->event->content->type]['talktime'] += $event->event->content->talktime : $result[$event->agent]['parciais'][$date][$event->event->content->type]['talktime'] = $event->event->content->talktime;

												// times talktime, ringtime and duration
												if(!isset($event->event->content->ringtime)){
														$event->event->content->ringtime = 0;
												}
												(isset($result[$event->agent]['total'][$event->event->content->type]['ringtime'])) ? $result[$event->agent]['total'][$event->event->content->type]['ringtime'] += $event->event->content->ringtime : $result[$event->agent]['total'][$event->event->content->type]['ringtime'] = $event->event->content->ringtime;
												(isset($result[$event->agent]['parciais'][$date][$event->event->content->type]['ringtime'])) ? $result[$event->agent]['parciais'][$date][$event->event->content->type]['ringtime'] += $event->event->content->ringtime : $result[$event->agent]['parciais'][$date][$event->event->content->type]['ringtime'] = $event->event->content->ringtime;

												(isset($result[$event->agent]['total'][$event->event->content->type]['duration'])) ? $result[$event->agent]['total'][$event->event->content->type]['duration'] += $event->event->content->duration : $result[$event->agent]['total'][$event->event->content->type]['duration'] = $event->event->content->duration;
												(isset($result[$event->agent]['parciais'][$date][$event->event->content->type]['duration'])) ? $result[$event->agent]['parciais'][$date][$event->event->content->type]['duration'] += $event->event->content->duration : $result[$event->agent]['parciais'][$date][$event->event->content->type]['duration'] = $event->event->content->duration;

												// analytic
												$result[$event->agent]['analytic'][$x]['eventdate'] = $eventdate;
												$result[$event->agent]['analytic'][$x]['status'] = $event->event->content->status;
												$result[$event->agent]['analytic'][$x]['class'] = self::labelEvent($event->event->content->status);
												$result[$event->agent]['analytic'][$x]['queue'] = $event->event->content->queue;
												$result[$event->agent]['analytic'][$x]['ringtime'] = QueueManager_Functions::fmt_Time($event->event->content->ringtime);
												$result[$event->agent]['analytic'][$x]['talktime'] = QueueManager_Functions::fmt_Time($event->event->content->talktime);
												$result[$event->agent]['analytic'][$x]['duration'] = QueueManager_Functions::fmt_Time($event->event->content->duration);

												if(isset($event->event->content->to)){
														$result[$event->agent]['analytic'][$x]['to'] = $event->event->content->to;
												}

												if(isset($event->event->content->from)){
														$result[$event->agent]['analytic'][$x]['from'] = $event->event->content->from;
												}

										}elseif($event->event->type == "feature"){

												$result[$event->agent][$event->event->type][$date]['header']['worktimelogin'] = $agent[0]->worktime->start;
												$result[$event->agent][$event->event->type][$date]['header']['worktimelogoff'] = $agent[0]->worktime->end;
												// if($event->event->content->type == 'login'){
												//     if(!isset($result[$event->agent][$event->event->type][$date]['header']['firstlogin'])){
												//         $result[$event->agent][$event->event->type][$date]['header']['firstlogin'] = $eventdate;
												//         //$data1 = new DateTime($eventdate);
												//     }
												// }elseif($event->event->content->type == 'logoff'){
												//     $result[$event->agent][$event->event->type][$date]['header']['lastlogoff'] = $eventdate;
												//     //$data2 = new DateTime($eventdate);
												// }elseif($event->event->content->type == 'pause'){
												//     $result[$event->agent][$event->event->type][$date]['header']['contpause'] = (isset($result[$event->agent][$event->event->type][$date]['header']['contpause'])) ? $result[$event->agent][$event->event->type][$date]['header']['contpause'] : 1;
												// }

												$result[$event->agent][$event->event->type][$date][$hour]['event'] = $event->event->content->pause_desc;
												$result[$event->agent][$event->event->type][$date][$hour]['class'] = self::labelEvent($event->event->content->type);
												$result[$event->agent][$event->event->type][$date][$hour]['queue'] = $event->event->content->queue;

										}

								}
						}
				}

				$result = json_encode($result);
				return $result;
		}

}
