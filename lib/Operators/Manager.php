<?php

/**
 * Class to manager Operators with mongodb
 *
 * @see Q-Manager
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 Opens Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 *
 */
class Operators_Manager {

    /**
     * Method to get all operators
     * @return <array>
     */
    public static function getAll($url) {

        $http = curl_init($url."?orderby=agent");
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
        $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
        $http_response = curl_exec($http);
        $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        switch ($httpcode) {
            case 200:
                $data = json_decode($http_response);
                break;
            default:
                $data = $httpcode;
                break;
        }

            return $data;
    }

    /**
     * Method to get agent by code
     * @param <int> $id
     * @return <array>
     */
    public static function getAgentByCode($url) {

        $http = curl_init($url);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, false);
        $status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_setopt($http, CURLOPT_RETURNTRANSFER,1);
        $http_response = curl_exec($http);
        $httpcode = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        switch ($httpcode) {
            case 200:
                $data = json_decode($http_response);
                break;
            default:
                $data = $httpcode;
                break;
        }

            return $data;
    }

    /*
    * Add operator
    * @param <array> $agent
    */
    public static function add($agent, $url) {

        if($agent['peer_fixed'] == 'on'){
            $fixed = true;
            $endpoint = "SIP/".$agent['code'];
        }else{
            $fixed = false;
            $endpoint = "";
        }

        $data = array("agent" => $agent["code"], "secret" => $agent["secret"], "endpoint" => $endpoint, "name" => $agent["name"], "email" => $agent["email"], "peer_fixed" => $fixed, "queues" => "","created" => date('Y-m-d H:i:s'), 'updated' => date('Y-m-d H:i:s'));

        foreach($agent["queuebox"] as $x => $value){
            $queue[$value]["pause"] = "0";
            $queue[$value]["state"] = "off";
            $queue[$value]["penalty"] = $agent["prioritybox"][$x];
        }

        $data['queues'] = $queue;
        $data["worktime"]["start"] = $agent["start"];
        $data["worktime"]["end"] = $agent["end"];

        $content = json_encode($data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;

    }

    /*
    * Add operators multiples
    * @param <array> $agent
    */
    public static function addMult($code, $dados, $url) {

        if($dados['peer_fixed'] == 'on'){
            $fixed = true;
            $endpoint = "SIP/".$code;
        }else{
            $fixed = false;
            $endpoint = "";
        }

        $data = array("agent" => $code, "secret" => $code.$code, "endpoint" => $endpoint, "peer_fixed" => $fixed, "name" => "Operador ".$code, "queues" => "","created" => date('Y-m-d H:i:s'), 'updated' => date('Y-m-d H:i:s'));

        foreach($dados["queuebox"] as $x => $value){
            $queue[$value]["pause"] = "0";
            $queue[$value]["state"] = "off";
            $queue[$value]["penalty"] = $dados["prioritybox"][$x];
        }

        $data['queues'] = $queue;
        $data["worktime"]["start"] = $dados["start"];
        $data["worktime"]["end"] = $dados["end"];
        $content = json_encode($data);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;

    }

    /**
     * Method to update a agent data
     * @param <Array> $agent
     */
    public function edit($agent, $url) {

        if($agent['peer_fixed'] == 'on'){
            $fixed = true;
            $endpoint = "SIP/".$agent['code'];
        }else{
            $fixed = false;
            $endpoint = "";
        }

        $data = array("agent" => $agent["code"], "secret" => $agent["secret"], "endpoint" => $endpoint, "peer_fixed" => $fixed, "name" => $agent["name"], "email" => $agent["email"], "queues" => "", 'updated' => date('Y-m-d H:i:s'));

        foreach($agent["queuebox"] as $x => $value){
            $queue[$value]["pause"] = "0";
            $queue[$value]["state"] = "off";
            $queue[$value]["penalty"] = $agent["prioritybox"][$x];
        }

        $data['queues'] = $queue;
        $data["worktime"]["start"] = $agent["start"];
        $data["worktime"]["end"] = $agent["end"];

        $content['data'] = $data;
        $content['filter']['agent'] =  $agent["code"];
        $content = json_encode($content);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;

    }

    /**
     * Method to remove a agent
     * @param <int> $id
     */
    public function remove($id, $url) {

        $array = array("agent" => $id);
        $content = json_encode($array);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;
    }

}
