<?php

/**
 * Q-Manager Panel Controller
 *
 * @category  Snep
 * @package   Q-Manager
 * @copyright Copyright (c) 2016 OpenS Tecnologia
 * @author    Tiago Zimmermann <tiago.zimmermann@opens.com.br>
 */
class SnepQManager_PanelController extends Zend_Controller_Action {

    public function preDispatch() {
        $this->_helper->layout()->setLayoutPath(realpath(dirname(__FILE__) . "/../views/layouts"));
        $this->_helper->layout()->setLayout("empty");
    }

    /**
     * Redirect page for fop2 interface
     *
     */
    public function indexAction() {

        $panel = "http://".$_SERVER["HTTP_HOST"].':8080/panel';
        $this->_redirect($panel);
    }

}

?>
